import "./styles/App.css";
import Header from "./components/Header";
import Card from "./components/Card";
import { Component } from "react";
import LanguageFilter from "./components/LanguageFilter";

class App extends Component {
  state = {
    jobs: [],
    loading: true,
    url: "https://jobhunt-api.herokuapp.com/jobs",
  };

  componentDidMount() {
    fetch(this.state.url)
      .then((response) => response.json())
      .then((response) => this.setState({ jobs: response, loading: false }));
  }

  componentDidUpdate(_, prevState) {
    if (this.state.url !== prevState.url) {
      fetch(this.state.url)
        .then((response) => response.json())
        .then((response) => this.setState({ jobs: response }));
    }
  }

  handleClick = (language) => {
    this.setState({
      url: `https://jobhunt-api.herokuapp.com/jobs?description=${language}`,
    });
  };

  render() {
    const { jobs, loading } = this.state;

    return (
      <>
        <div className="App">
          <Header />
          <LanguageFilter handleClick={this.handleClick} />
          <h2>🔥 Trending Jobs</h2>
          <div className="card-list">
            {loading && (
              <div className="loader-container">
                <div id="loader"></div>
              </div>
            )}
            {jobs.map((job) => (
              <Card job={job} />
            ))}
          </div>
        </div>
      </>
    );
  }
}

export default App;
