import { Component } from "react";
import "./styles.css";
import {
  FaJs,
  FaSketch,
  FaJava,
  FaPython,
  FaReact,
  FaList,
} from "react-icons/fa";
import { SiCsharp } from "react-icons/si";

class LanguageFilter extends Component {
  render() {
    const { handleClick } = this.props;
    return (
      <div className="filter">
        <button onClick={() => handleClick("javascript")}>
          <FaJs className="icon" size={20} color="#fff" />
          <span>JavaScript</span>
        </button>
        <button onClick={() => handleClick("ruby")}>
          <FaSketch className="icon" size={20} color="#fff" />
          <span>Ruby</span>
        </button>
        <button onClick={() => handleClick("python")}>
          <FaPython className="icon" size={20} color="#fff" />
          <span>Python</span>
        </button>
        <button onClick={() => handleClick("c#")}>
          <SiCsharp className="icon" size={20} color="#fff" />
          <span>C#</span>
        </button>
        <button onClick={() => handleClick("java")}>
          <FaJava className="icon" size={20} color="#fff" />
          <span>Java</span>
        </button>
        <button onClick={() => handleClick("react")}>
          <FaReact className="icon" size={20} color="#fff" />
          <span>React</span>
        </button>
        <button onClick={() => handleClick("")}>
          <FaList className="icon" size={20} color="#fff" />
          <span>All Jobs</span>
        </button>
      </div>
    );
  }
}

export default LanguageFilter;
